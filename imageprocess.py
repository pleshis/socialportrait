#!/usr/bin/env python
# -*- coding: utf-8 -*- 

import numpy as np
import math as math
import sys as sys



# create super pixels from input image
# return smaller RGB image
def superPixels(image,superPixelSize):
	
	print 'super pixels'

	xMax = image.shape[1];
	yMax = image.shape[0];
	print '  input resolution:',xMax,'x',yMax;

	lastPercent = 0
	imageOut = np.zeros((yMax/superPixelSize,xMax/superPixelSize,3))

	for x in xrange(0,xMax-1):
		for y in xrange(0,yMax-1):
			imageOut[y/superPixelSize][x/superPixelSize] = image[y][x]

		# informace o stavu
		percent = int((float(x)/(xMax-1)*100.0))
		if(percent!=lastPercent):
			lastPercent = percent
			print '{0:3d}%'.format(percent)

	print '100%'
	return imageOut



# transform image to grayScale
# return grayscale image
def grayScale(image,invertion):

	print 'grayscale'

	xMax = image.shape[1];
	yMax = image.shape[0];
	print '  grays resolution:',xMax,'x',yMax;

	imageOut = np.zeros((yMax,xMax))

	for x in xrange(0,xMax-1):
		for y in xrange(0,yMax-1):

			if(invertion):
				imageOut[y][x] = 1.0-((image[y][x][0]+image[y][x][1]+image[y][x][2])/3.0)
			else:
				imageOut[y][x] = (image[y][x][0]+image[y][x][1]+image[y][x][2])/3.0

	return imageOut


# quantizate color to N levels
def quantization(image,levels):
	
	print 'quantization to',len(levels)+1,'levels'

	xMax = image.shape[1];
	yMax = image.shape[0];
	imageOut = np.zeros((yMax,xMax))

	#more than maximum
	levels.append(1.1)

	for x in xrange(0,xMax-1):
		for y in xrange(0,yMax-1):

			lastLevel = 0
			levelIndex = 0

			#put pixel to right bucket
			for level in levels:
				if((image[y][x]>=lastLevel)and(image[y][x]<level)):
					imageOut[y][x] = levelIndex
					break

				lastLevel = level
				levelIndex+=1

	return imageOut


#find clusters and indexize pixels in them
def findClusters(image, maxRecursion):

	print 'finding clustures'

	xMax = image.shape[1];
	yMax = image.shape[0];
	clusters = np.zeros((yMax,xMax))
	clusterIntenzity = np.zeros(yMax*xMax)
	clusterID = 1
	sys.setrecursionlimit(maxRecursion)

	for x in xrange(0,xMax-1):
		for y in xrange(0,yMax-1):
			if(clusters[y][x]==0):
				floodFill(image,clusters,[y,x],image[y][x],clusterID)
				clusterIntenzity[clusterID] = image[y][x]
				clusterID +=1

	clusterCount = clusterID

	return [clusters, clusterIntenzity[0:clusterID]]


# flood filling
def floodFill(image, clusters, nodeCoord, targetColor, replacementCluster):

	if(image[nodeCoord[0]][nodeCoord[1]]!=targetColor):
		return
	if(clusters[nodeCoord[0]][nodeCoord[1]]!=0):
		return

	clusters[nodeCoord[0]][nodeCoord[1]] = replacementCluster

	yMax = image.shape[0];
	xMax = image.shape[1];

	for dCoord in [[+1,0],[-1,0],[0,+1],[0,-1]]:

		cy = nodeCoord[0] + dCoord[0]
		cx = nodeCoord[1] + dCoord[1]

		# ranges
		if((cx<0)or(cy<0)or(cx>xMax-1)or(cy>yMax-1)):
			continue

		floodFill(image,clusters,[cy,cx],targetColor,replacementCluster)


# filter clusters
def importantClusters(clusters,clustersIntenzities,threshold):

	cc = clustersIntenzities.shape[0]

	xMax = clusters.shape[1];
	yMax = clusters.shape[0];

	clusterCount = np.zeros(cc)
	clusterMap   = np.zeros(cc)
	clusterIntenzity = np.zeros(cc)

	# create histogram
	for x in xrange(0,xMax-1):
		for y in xrange(0,yMax-1):
			clusterCount[clusters[y][x]] += 1

	# creation of new distribution
	# for practical issues are clusters numbered from begining
	importantClusters = np.zeros((yMax,xMax))
	newClusterID = 1
	for x in xrange(0,xMax-1):
		for y in xrange(0,yMax-1):
			if(clusterCount[clusters[y][x]]>=threshold):

				#new mapping
				if(clusterMap[clusters[y][x]]==0):
					oldClusterID = clusters[y][x]
					clusterMap[clusters[y][x]] = newClusterID
					clusterIntenzity[newClusterID] = clustersIntenzities[oldClusterID]
					newClusterID += 1

				importantClusters[y][x] = clusterMap[clusters[y][x]]

	return [importantClusters, clusterIntenzity[0:newClusterID]]



# testing function for redrawing
def clusters2intenzity(clusters,clusterIntenzity):

	xMax = clusters.shape[1];
	yMax = clusters.shape[0];

	image = np.zeros((yMax,xMax))

	for x in xrange(0,xMax-1):
		for y in xrange(0,yMax-1):
			if(clusters[y][x]==0):
				image[y][x] = -1.0
			else:
				image[y][x] = clusterIntenzity[clusters[y][x]]

	return image



#!/usr/bin/env python
# -*- coding: utf-8 -*- 

import numpy as np
import math as math
import sys as sys
import random as random
import string

from PIL import Image
from PIL import ImageFont
from PIL import ImageDraw

import _imaging
import Image 


#create new image
def newImage(resolution, backColor):

	img = Image.new("RGB", (resolution[1],resolution[0]), backColor)
	draw = ImageDraw.Draw(img)


	return [img, draw];


#save image
def saveImage(img,resolution,outFileName):

	print 'saving picture'

	img = img.resize((int(resolution[1]/2.0),int(resolution[0]/2.0)), Image.ANTIALIAS)
	img.save(outFileName)

	return


#draw label
def drawLabel(draw,rect,intenzity,maxIntenzity,defaultColor,superPixelSize,dictionary):


	intenzity = (intenzity/(maxIntenzity-1))

	# remove black areas
	# if((intenzity<=0.0)):
	# 	return

	# consts (space for improovement)
	#			  10	20	30	40	50	60		# px
	fontSizes = [  14,   26,   38,   50,   62,   74]	# points
	fontRatio = [0.79, 1.58, 2.26, 2.92, 3.63, 4.20]	# y/x

	height = rect[3]
	width  = rect[2]

	text = getWord(int((width)/fontRatio[height-1]),height,dictionary)
	
	color = (int(defaultColor[0]*intenzity),
			 int(defaultColor[1]*intenzity),
			 int(defaultColor[2]*intenzity))

	font = ImageFont.truetype("Courier.ttf", fontSizes[height-1])
	draw.text((rect[0]*superPixelSize*2, rect[1]*superPixelSize*2),text,color,font=font)

	return;


def prepareDict(fileName):

	dictionary = []
	longestWord = 0

	with open(fileName, 'r') as f:
		lines = f.readlines()
		rawLabels = []
		for line in lines:
			line = line.replace('\n', ' ')
			line = line.replace('\t', ' ')
			words = line.split(' ')

			for word in words:
				rawLabels.append(word)
				if(len(word)>longestWord):
					longestWord = len(word)


	for i in xrange(0,longestWord):
		dictionary.append([])

	for word in rawLabels:
		if(len(word)<1):
			continue

		word = word.replace('_', ' ')
		dictionary[len(word)-1].append(word)

	return dictionary


#return random word with appropriate length
def getWord(length,height,dictionary):

	#todo: if no record in dictionary for length then split

	if(length<=0):
		return ''

	#proper return
	if(length<=len(dictionary)):
		return dictionary[length-1][random.randint(0,len(dictionary[length-1])-1)]

	#split word recursively to two
	index = random.randint(2,length-2)
	return getWord(index,height,dictionary) + ' ' + getWord(length-index-1,height,dictionary)

	#for tests
	chars = string.ascii_uppercase
	chars = str(unichr(48+height))
	text  = chars[random.randint(0,len(chars)-1)] * length
	return text 

#!/usr/bin/env python
# -*- coding: utf-8 -*- 


import matplotlib.pyplot as plt
import matplotlib.image as mpimg

from imageprocess import *
from mapping import *
from drawing import *



#settings
imageName = 'imageIn.png'		# nacitany obrazek
dictFileName = 'dict.txt'		# nacitany slovnik
quantizationLevels = 5			# zhruba 3..10
invertionInput  = False			# inverze barev vstupu
invertionOutput = False			# inverze barev vystupu
outFileName  = 'imageOut.jpg'	# nazev vystupniho obrazku
takeClustersBiggerThan = 5		# lze vyradit male clustriky
superPixelSize = 5 				# velikost superpixelu
maxHeight = 5					# 1..6 (ideal tak 3)
maxRecursion = 100000			# pokud moc velke plochy, tak i vetsi
defaultColor = (150,190,255)	# max barva (do bela)
# backColor    = (  0,  0, 50)	# barva pozadi

if(invertionOutput):
	backColor = tuple([int(x * (1.0-(1.0/quantizationLevels)/2.0)) for x in defaultColor])
else:
	backColor = tuple([int(x * (1.0/quantizationLevels)/2.0) for x in defaultColor])

#preinit of image
imageIn=mpimg.imread(imageName)
image = superPixels(imageIn,superPixelSize)
image = grayScale(image,invertionInput)
image = quantization(image,[float(x)/quantizationLevels for x in range(1,quantizationLevels)])

#clusters finding
[clusters,clusterIntenzity] = findClusters(image,maxRecursion)
print 'Old cluster count:',clusterIntenzity.shape[0]

[newClusters,newClusterIntenzity] = importantClusters(clusters,clusterIntenzity,takeClustersBiggerThan)
print 'New cluster count:',newClusterIntenzity.shape[0]

# #show raw clusters
# image = clusters2intenzity(clusters,clusterIntenzity)
# plt.figure()
# imgplot = plt.imshow(image,interpolation='nearest')
# imgplot.set_cmap('gray')
# plt.draw()

# #show best clusters
# newImage = clusters2intenzity(newClusters,newClusterIntenzity)
# plt.figure()
# imgplot = plt.imshow(newImage,interpolation='nearest')
# imgplot.set_cmap('gray')
# plt.draw()

#generate image and save
resolution = (image.shape[0]*superPixelSize*2,image.shape[1]*superPixelSize*2)
processClusters(outFileName,newClusters,newClusterIntenzity,quantizationLevels,resolution,defaultColor,superPixelSize,maxHeight,backColor,dictFileName)


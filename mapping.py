#!/usr/bin/env python
# -*- coding: utf-8 -*- 


import numpy as np
import math as math
import sys as sys
import random as random

from drawing import *


#process clusters
def processClusters(outFileName,clusters,clusterIntenzity,maxIntenzity,resolution,defaultColor,superPixelSize,maxHeight,backColor,dictFileName):

	random.seed()

	[img, draw] = newImage(resolution,backColor)
	dictionary = prepareDict(dictFileName)

	# process clusters
	for clusterID in xrange(1,clusterIntenzity.shape[0]):

		#process rectangles in cluster
		for rect in processCluster(clusters,clusterID,maxHeight=maxHeight):
			drawLabel(draw,rect,clusterIntenzity[clusterID],maxIntenzity,defaultColor,superPixelSize,dictionary)

	saveImage(img,resolution,outFileName)

	return


def processCluster(clusters,clusterID,maxHeight):

	print 'processing cluster no.',clusterID

	xMax = clusters.shape[1];
	yMax = clusters.shape[0];

	image = np.zeros((yMax,xMax))

	#find convex rectangle
	minX = float('+inf')
	minY = float('+inf')
	maxX = float('-inf')
	maxY = float('-inf')

	for x in xrange(0,xMax-1):
		for y in xrange(0,yMax-1):
			if(clusters[y][x]==clusterID):
				if(x<minX):
					minX = x
				if(y<minY):
					minY = y
				if(x>maxX):
					maxX = x
				if(y>maxY):
					maxY = y

	
	#get smaller rects
	rects = processCell(clusters[minY:maxY+1,minX:maxX+1],clusterID,maxHeight)

	#remap back to global space
	rectsOut = []
	for rec in rects:
		rectsOut.append([rec[0]+minX,rec[1]+minY,rec[2],rec[3]])

	return rectsOut


def processCell(cell,clusterID,maxHeight):

	xMax = cell.shape[1];
	yMax = cell.shape[0];

	#some const for miss count (space for improvement)
	#   bigger number has less holes but take more time
	maxMissCount = xMax*yMax*2	

	rectangles = []
	missCount = 0

	#while blank spaces exists
	while(1):

		tmpMaxHeight = random.randint(1,maxHeight)

		# random shoot
		x = random.randint(0,xMax-1)
		y = random.randint(0,yMax-1)

		#miss the cluster :(
		if(cell[y][x]!=clusterID):
			missCount += 1
			if(missCount>=maxMissCount):
				break
			continue
		missCount = 0


		rec = [x,y,1,1]	#x,y,w,h	

		# make rectangle larger
		while(1):

			changesX = 0
			changesY = 0

			if(spreadRight(cell,clusterID,rec)):
				rec = [rec[0],rec[1],rec[2]+1,rec[3]]
				changesX += 1

			if(spreadLeft(cell,clusterID,rec)):
				rec = [rec[0]-1,rec[1],rec[2]+1,rec[3]]
				changesX += 1

			if(rec[3]<tmpMaxHeight):
				if(spreadBottom(cell,clusterID,rec)):
					rec = [rec[0],rec[1],rec[2],rec[3]+1]
					changesY += 1

			if(rec[3]<tmpMaxHeight):
				if(spreadTop(cell,clusterID,rec)):
					rec = [rec[0],rec[1]-1,rec[2],rec[3]+1]
					changesY += 1

			# no change
			if((changesX==0)and(changesY==0)):

				# redraw rect to non active area
				for tmpX in xrange(rec[0],rec[0]+rec[2]):
					for tmpY in xrange(rec[1],rec[1]+rec[3]):
						cell[tmpY][tmpX] = 0;

				# add rect
				rectangles.append(rec)
				break

	return rectangles



def spreadRight(cell,clusterID,rec):
	if(cell.shape[1]<=rec[0]+rec[2]):
		return False
	for y in xrange(rec[1],rec[1]+rec[3]):
		if(cell[y][rec[0]+rec[2]]!=clusterID):
			return False
	return True

def spreadLeft(cell,clusterID,rec):
	if(0>rec[0]-1):
		return False
	for y in xrange(rec[1],rec[1]+rec[3]):
		if(cell[y][rec[0]-1]!=clusterID):
			return False
	return True

def spreadBottom(cell,clusterID,rec):
	if(cell.shape[0]<=rec[1]+rec[3]):
		return False
	for x in xrange(rec[0],rec[0]+rec[2]):
		if(cell[rec[1]+rec[3]][x]!=clusterID):
			return False
	return True

def spreadTop(cell,clusterID,rec):
	if(0>rec[1]-1):
		return False
	for x in xrange(rec[0],rec[0]+rec[2]):
		if(cell[rec[1]-1][x]!=clusterID):
			return False
	return True

